# CAN FD : Controller Area Network Flexible Data

* Permet de changer dynamiquement le débit
* Permet de changer dynamiquement la taille des données envoyé :D
* ==> Donc plus rapide car plus de données dans les trames
* De 11 à 29 bit pour la priorité / identification
* 5 fois plus rapide que CAN classique
* Meilleur détection d'erreurs
* Champs données de 8 à 64 octect

###########################################################################################################################################################################
###########################################################################################################################################################################
###########################################################################################################################################################################

# Le protocole CAN Bus : Controller Area Network

Spécificité générale :
* Broadcast = on ne peut cibler l'envoi vers un destinataire = MAIS --> les récepteurs peuvent filtrer les messages
* Système de transmission : Non Retour à Zero (remember en IUT) = NRZ
* Les messages possèdes une priorité

## Type de message 

### Data Frame : hello les potos, tenez de la données bande de drogé !

Contenu :
* 'Arbitration Field' : détermine la priorité et/ou le plus souvent le type de contenu du msg
* Pour CAN 2.0A --> identifiant de 11 bit et 1 bit à 1 (dominant) pour dire que c'est pas une question
* Pour CAN 2.0B --> identifiant de 29 bit et 1 bit à 1 (dominant) pour dire que c'est pas une question
* Données : 0 à 8 octet
* CRC : 15 bit de checksum
* ACK : 1 bit


### Remote Frame : Hello les potos, quelqu'un peut m'envoyer de la data j'suis en manque ?

Remote Frame = Data Frame MAIIIIIS ya pas de données et le bit à 1 de toute à l'heure est à 0 (récessif)

* Utilisé pour les FAQ
* Le champs donnée doit être équivalent à la taille de se que tu attend comme taille de réponse

### Error Frame : cé la merde, faut ressayer
Ce type de message viole les règles de CAN --> flag de 6 fois le meme bit = nul
Permet d'envoyer à tout le monde pour indiquer qu'on à détecté une erreure --> l'emmeteur va alors renvoyer le msg.


### Overload Frame : wooooo les gars on peut se calmer jpp moi de lire vos msgs

Envoyé quand t'es trop un mec busy --> c'est obsolete comme type de msg donc osef

## Etablissement des priorité / identifiant de msg

Dès que un contrôleur CAN détecte un bus idle -> il va envoyer un msg = ça peut faire des colisions.
Si un réception détecte qu'un autre est domimant alors il va se mettre en réception.

## Adresser un msg et identifier un msg

Comme expliqué précedement il n'y a pas de champs @src et @dst --> donc tu utilises des filtres (comme en tp).
Par contre utilisé le champs de priorité comme identifiant source c'est méga commun (comme en tp).

## La rapidité
* Le débit max de CAN bus est de 1 Mbit/s
* Le débit min de CAN est de 10 kbit/s sans soucis

## Distance 
* Entre 100 mètre (500kbit/s) et 6 kilomètre (10kit/s)

## Connecteur Bus et câble
* Il y a un reflecteur = enlever les bruits = résistance 
* Un contrôleur d'alimentation
* Généralement câble avec 9, 6 ou 5 pin

## Le timing c'est bien cool
* Le temps / le sampling est divisé en 4 groupes :
    * Segement de syncronization : le temps utilisé pour être syncro
    * Segment de propagation : le temps pour composer le délai de la liaison bus
    * Segment de phase x2 : temps variable -> utililisé aussi pour syncro l'hologue = en gros c'est du bourrage

## Le traitment des erreurs
Le traitement des erreurs est intégré dans le protocole CAN et est d'une grande importance pour les performances d'un système CAN.
Le traitement des erreurs vise à détecter les erreurs dans les messages apparaissant sur le bus CAN, afin que l'émetteur puisse retransmettre un message erroné.

Chaque noeud / contrôleur disposition d'un ensemble de compteur : si ça dépasse une certaines valeur le gars est mis de côté
* compteurs de réception
* compteurs d'émission

### La suveillance de bit
En gros tu prévoir d'envoyer un certains niveau (NRZ) et en sortie tu contrôles que tu envoie bien ça --> et si c'est pas le bon alors une erreure est signalé

### Bit Stuffind
Si plus de cinq bits consécutifs de même niveau se produisent sur le bus, une Stuff Error est signalée. --> classique shit

### Vérification format frame : Frame Check 
Si un contrôleur CAN détecte une valeur non valide dans l'un de ces champs fixes, une erreur Frame check est signalé --> càd genre tu respecte pas la norme

### Vérification acknowlegement
Pour répondre un ACK tu envois dans la partie spécifié un bit dominant --> si tu envoi un bit récessif = une erreur

### Vérification du CRC
Si erreur CRC = erreur



###########################################################################################################################################################################
###########################################################################################################################################################################
###########################################################################################################################################################################



## FAQ 

* Est-ce que le bus CAN peut avoir plusieurs maîtres ?
    * NON -> 1 à la fois parle sinon c'est le BOOOORDEL

* Peut-on mixer du CAN 2.0A et du CAN 2.0B sur un même BUS ?
    * OUI

* Si une trame ne contient pas de données, ça peut-être parce que  ?
    * DLC = 0 --> DLC = taille du champs de données
    * RTR = 0 --> = 0 si c'est une quesiton 

* Combien de données peut-on transférer dans une trame CAN (hors CAN-FD, hors identifiant)
    * 8 octet

* Combien de données peut-on transférer dans une trame CAN-FD (hors identifiant)
    * 64 octet

* Calculez le "rendement" d'une trame CAN2.0B avec 8 octets de données 
    * 29 bit id + 1 bit SPOD + 6 bit command + data 64 bit + crc 16 bit + ack 16 bit + eof 7 bit = 125 /64 = rendement de 0,5 environ 
    * Rendement = Nb de bits de données / Nb de bits transmis



* Le bus CAN utilise quel type de bus ?
    * Broadcast

* Combien y a t'il de type de message définit dans le standard CAN ?
    * 4 types

* Quel outil nous aide à calculer les paramètres du bus et des registres du bus CAN ?
    * C'est le Bit Timing Calculator

* Il existe quatre type de frame c'est lesquels ?
    * Data Frame
    * Remote Frame
    * Error Frame
    * Overload Frame

* Quel trame demande de l'information au bus ?
    * Remote

* Quel est le type de trame CAN le moins fréquent ?
    * Overload

* Quel controlleur CAN est autorisé à émmetteur sur le bus CAN ?
    * Tout les contrôlleur CAN

* Quand deux ou plus controlleurs commence une communication au même moment, un contrôleur gagne avec la manche en envoyant un bit récessif (état logique 1) alors que les autres envoient un bit dominant (état logique 0), est-ce vrai ?
    * FAUX c'est le bit dominant qui GG :D pouvoir aux zéro

* CAN utilise les adresse des message pour déterminer où envoyer ces messages ?
    * MEGA FAUX --> ya pas d'adressage avec CAN

* FULL CAN (CAN CAN 2.0B) et Basic CAN (CAN 2.0A) sont complémentement compatible ?
    * Oui

* Quel est l'encodage des bits utilisé ?
    * NZR = Non retour à Zero

* Combien de files sont utilisé avec la CAN vitesse éclaire ?
    * 2

* Avec une vitesse de 1 Mbit/s quel est la distance de câble max avec le protocole CAN ?
    * Environ 40 mètres

* La norme ISO 118989 CAN doit toujours faire quoi quelque soit sa vitesse ?
    * Etre terminée

* Exist'il un standard pour les connecteurs CAN ?
    * OUI = ouf

* Quel est la taille du segement de Syncronization ?
    * 1 quanta
    * Le « Time Quantum » est l'unité de temps construite à partir de la période de l'oscillateur interne de chaque nœud. La fréquence du bus étant au maximum de 1 MHz et celles des oscillateurs de plusieurs MHz, le « Time Quantum » vaut généralement plusieurs périodes d'horloge (entre 1 et 32 fois). 

* Quand on ajuste la durée d'un bit est appelée « Nominal Bit Time », l'ajustement est fait avec ?
    * Le maximum

* La plupart des contrôleur CAN permettent aux programmeur de définir le bit d'horlogue en utilisant plusieurs paramètres, incluant :
    * Un prescripteur d'horlogue

* Chaque bit du CAN bus doit être divisé en combien de segment ?
    * 4

* Seulement certains contrôleur CAN permettent de détecter des erreurs ?
    * FAUX

* Chaque noeud maintien deux compteurs d'erreurs, lesquels ?
    * Transmit Error  = compteur d'erreur d'émission
    * Receive Error Counter = compteur d'erreur de réception

* Au bout de combien de bit il faut ajouter un bit avec un polarité inverse, cette technique est appelé bit stuff :
    * 5

* Tout les contrôleurs qui ont bien reçu le msg vont envoyé un level domimant (0) qui informé de la bonne réception : ACK, est-ce vrai ?
    * VRAI




###########################################################################################################################################################################
###########################################################################################################################################################################
###########################################################################################################################################################################



## Véhicule autonome

### Différents type d'autonomo=ie
Il existe 6 types d'autonomie :
* Niveau 0 : 
    * Conducteur : Le boss
    * Véhicule : Le conducteur est le boss de tout
* Niveau 1 : 
    * Conducteur : Le boss 99% du temps
    * Véhicule : Peut freiner et accélérer dans certains cas et piloter un chouia
* Niveau 2 :
    * Conducteur: Le boss 80 % du temps
    * Véhicule : Peut freiner et accélérer dans certains cas et piloter un chouia
* Niveau 3 :
    * Conducteur : pépouse, réagit seulement aux alertes
    * Véhicule : Freiner, accélere, diriger, reconnaitre ses limites
* Niveau 4 : 
    * Conducteur : useless le conducteur, tu sers à R
    * Véhicule : Il fait le café mais ya quand même des limites (genre décider si on but le gamin)
* Niveau 5 :
    * Conducteur : useless le conducteur, tu sers à R
    * Véhicule : Il fait très bien le café et les pipes

### Les différents capteurs

#### Leurs applications
* Ultrasons :
    * Aide au parkage
* Radar :
    * Changement de voie
    * Détection des feux
    * Détection collisions
* Lidar :
    * Cartographie de l'environnement

### L'ordinateur centrale
* Faut que ça patate sec niveau débit
* Faible conso énergitique
* Robustre : un chaud / froid ne lui fait pas peur

### Le réseau dans ta vago
* Ethernet : ton porno préféré, les capteurs, la sélection des vitesse, airbag, direction --> ton porno, l'écran, l'audio, le gpqs
* CAN : système de détection de collision, les capteurs, les vitesses, airbag, direction --> surtout contrôle feux, température, porte
* FlexRay : les capteurs, direction, airbag, vitesses
* LIN : lumières, température, porte
* MOST : idem ethernet

Bon tu peux mettre un peu se que tu veux sauf certains éléments ou faut faire gaffe.

#### Du plus rapide au plus lent et du coup du plus cher au bon coin
* Automotive Ethernet
* MOST
* FlexRay
* CAN-FD / CAN
* PSI5
* LIN /SENT






###########################################################################################################################################################################
###########################################################################################################################################################################
###########################################################################################################################################################################





#  Les capteurs et tout se qui va avec

## Nos besoins
* Une certaines portée, adaptée à la vitesse
* Un bon champs de vision 

## Les différents types de capteurs
### Caméra
* Reconnaissance d'image : sachet de plastique
* Par beau temps pour voir le plus possible

### Caméra thermique
Cool :
* Nuit
* Nuit / Pluie


### Radar
Cool :
* Détecter des formes par mauvais temps
* Voir le plus loin possible par mauvais temps
* Mesure de la vitesse d'autre véhicule en 1 mesure

### Lidar
Pas cool :
* Bien trop chère mon gars

## Problématiques
Comment faire pour palier à des possibles problèmees d'interférences ? 
* Multiplexage en fréquence
* Ré-allocation dynamique des fréquence
* Diminuer le résolution des fréquences pour mieux partager le spectre


###########################################################################################################################################################################
###########################################################################################################################################################################
###########################################################################################################################################################################




# La fameuse FAQ

On veut détecter des gens de nuit par temps de pluie. Que est le meilleur capteur ?
* Caméra tehermique

On veut détecter un arbre couché sur la voie (sans forcement reconnaître que c'est un arbre), de nuit par temps pluie. Quuel est le meilleur capteur ?
* Radar

La voiture fait une course de raylle dans la forêt après un bon orage. Quel est le meilleur capteur pour analyser les obstacles face aux véhicules ?
* Radar

Quel capteur utiliser pour ne pas piler devant un sachet plastique qui vole sur l'autoroute ?
* Caméra "classique"

Si il fallait faire un véhicule économiquement viable, quel capteur vaut-il mieux écarter ?
* Spinning Lidar

On veut 'voir' le plus loin possible par beau temps. Quel capteur choisir ?
* Caméra

On veut 'voir' le plus loin possible par mauvais temps. Quel capteur choisir ?
* Radar

On veut mesurer la vitesse des véhicules alentours en une seule mesure. Quel capteur choisir ?
* Radar

Au faite, c'est quoi l'acronyme SLAM ?
* Simultaneous Localization Mapping

Et dit moi tout sur l'egomotion ? Encore un truc de bobo gocho ?
* Nop. C'est l'estimation du mouvement d'un capteur en se référant à des mesures de référence.

Whesh pourquoi Tesla fait le fdp et utilise pas Lidar ?
* C'est aussi cher qu'une tesla et une IA c'est plus 2020

